import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="send-notification",
    version="0.0.1",
    author="Kevin Hussey",
    author_email="kevin.hussey@86400.com.au",
    description="A utility for sending SMS using a mustache template",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/kevinhussey-86400/send-notification",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts':
            ["sendsms=notification.sms_notification:main",
            "sendpush=notification.push_notification:main"]
    },
    # scripts=['send-notification'],
    install_requires=['pandas', 'pystache',
                     'PyInquirer >=1.0.3'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
