# send-notification

A command-line tool for sending bulk SMS and push notifications

The utility lets you define a mustache template and provide a CSV file with matching columns for bulk SMS sends.

## Installation

The utility requires `python3.7`

```
python -m pip install send-notification
```


## Running 

First, you need to define a mustache template and save to a file, e.g. test.mustache:

```
Hi {{first_name}}, this is a test SMS. 
```

Next, you need to create a bulk send CSV file. The file must have a `user_id` column. 

test.csv
```
"userId","first_name"
"test-uuid","Joe"
```

To process the batch file:

```
sendsms --template test.mustache test.csv scheduled-maintaince-31-07-2020

```

## Testing

Before processing a bulk CVS file, it's a good idea to sample some entries and send them to yourself first. Testing is useful to confirm the formatting and the number of segments are as expected.

To sample and override the mobile_number in the CSV file:

```
sendsms --template example.mustache --sample 1 --sendto +614XXXXXXXX test.csv scheduled-maintaince-31-07-2020
```
