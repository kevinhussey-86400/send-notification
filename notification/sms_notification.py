#!/usr/bin/env python
import argparse
from datetime import datetime, timedelta
import os
import requests
from os.path import expanduser
import configparser
from PyInquirer import prompt
import pandas as pd
import numpy as np
import pystache
from KCAuth import KCAuth

config_file = os.path.join(expanduser("~"), '.notification-api.config')

def config_api():
    questions = [
        {
            'type': 'input',
            'name': 'url',
            'message': 'url',
        }
    ]

    print("== Client Configuration ==")
    return prompt(questions)

def read_config(force_update=False):
    config = configparser.ConfigParser()

    if not force_update and os.path.isfile(config_file):
        config.read(config_file)
    else:
        config['api'] = config_api()

        print("Writing config to " + config_file)
        with open(config_file, 'w') as configfile:
            config.write(configfile)

    return config

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def send_bulk_sms(url, access_token, user_ids, body, notification_id):

    user_ids_chunks = chunks(user_ids.tolist(), 100)
    for ids in user_ids_chunks:
        send_sms(url, access_token, ids, body, notification_id)


def send_sms(url, access_token, user_ids, body, notification_id):
    try:
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
        payload = {
            'correlationId': notification_id,
            'notificationId': notification_id,
            'sms': {
                'body': body
            },
            'expiry': (datetime.now() + timedelta(hours=9)).isoformat(),
            'userIds': user_ids
        }
        print(payload)
        r = requests.post(url, headers=headers, data=payload)
        print('success ' + r.text)
    except:
        print('Error sending notification to : ')


def run(conf, file, template):
    df = pd.read_csv(file, dtype={'user_id': 'str'})
    parsed_template = None

    renderer = pystache.Renderer()
    with open(template, 'r') as template_file:
        parsed_template = pystache.parse(template_file.read())

    # Extract keys from the template for validation against input file
    template_keys = []
    for node in parsed_template._parse_tree:
        if isinstance(node, pystache.parser._EscapeNode):
            template_keys.append(node.key)

    for key in template_keys:
        if key not in df.columns:
            raise NameError('Missing column in input file ' + key)

    kcAuth = KCAuth("keycloak.json")
    access_token = kcAuth.getAccessToken()

    notification_id = input("Notification id (important) e.g. scheduled-maintenance on-boarding-nudge: ")
    timestamp = '-' + datetime.now().replace(microsecond=0).isoformat()

    if len(template_keys) == 0:
        print('sending bulk sms')
        message = renderer.render(parsed_template, {})
        send_bulk_sms(conf['url'], access_token, df['user_id'].to_numpy(), message, notification_id + timestamp)
    else:
        for index, row in df.iterrows():
            template_params = {}
            for key in template_keys:
                template_params[key] = row[key]

            message = renderer.render(parsed_template, template_params)
            user_id = row["user_id"]

            send_sms(conf['url'], access_token, [user_id], message, notification_id + timestamp)


def main():
    parser = argparse.ArgumentParser(prog='sms-notification')

    parser.add_argument(
        '--template', help='Mustache template to use.')
    parser.add_argument(
        'file', help='Input file with fields matching the template.')
    args = parser.parse_args()

    run(read_config()['api'], args.file, args.template)


if __name__ == '__main__':
    main()
